CREATE TABLE post_data (
			id SERIAL,
			mime_type VARCHAR,
			text VARCHAR,
			comment VARCHAR,
			CONSTRAINT post_data_id_pk
			  PRIMARY KEY (id)
);


CREATE TABLE requests (
			id SERIAL,
			method VARCHAR,
			url VARCHAR,
			http_version VARCHAR,
			post_data_id SERIAL,
			header_size INT,
			body_size INT,
			comment VARCHAR,
			CONSTRAINT requests_id_pk
			  PRIMARY KEY (id),
			CONSTRAINT requests_post_data_id_fk
			  FOREIGN KEY (post_data_id)
			  REFERENCES post_data (id)
);


CREATE TABLE cookies (
			id SERIAL,
			name VARCHAR,
			value VARCHAR,
			path VARCHAR,
			domain VARCHAR,
			expires VARCHAR,
			http_only BOOLEAN,
			secure BOOLEAN,
			comment VARCHAR,
			req_id SERIAL,
			CONSTRAINT cookies_id_pk
			  PRIMARY KEY (id),
			CONSTRAINT cookies_req_id_fk
			  FOREIGN KEY (req_id)
			  REFERENCES requests (id)
);



CREATE TABLE headers (
			id SERIAL,
			name VARCHAR,
			value VARCHAR,
			comment VARCHAR,
			req_id SERIAL,
			CONSTRAINT headers_id_pk
			  PRIMARY KEY (id),
			CONSTRAINT headers_req_id_fk
			  FOREIGN KEY (req_id)
			  REFERENCES requests (id)
);


CREATE TABLE query_string (
			id SERIAL,
			name VARCHAR,
			value VARCHAR,
			comment VARCHAR,
			req_id SERIAL,
			CONSTRAINT query_string_id_pk
			  PRIMARY KEY (id),
			CONSTRAINT query_string_req_id_fk
			  FOREIGN KEY (req_id)
			  REFERENCES requests (id)
);


CREATE TABLE params (
			id SERIAL,
			name VARCHAR,
			value VARCHAR,
			file_name VARCHAR,
			content_type VARCHAR,
			comment VARCHAR,
			post_data_id SERIAL,
			CONSTRAINT params_id_pk
			  PRIMARY KEY (id),
			CONSTRAINT params_post_data_id_fk
			  FOREIGN KEY (post_data_id)
			  REFERENCES post_data (id)
);


			