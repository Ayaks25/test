package DAO;

import java.util.List;
import domains.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.RequestMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
/**
 * Created by Алексей on 31.03.2016.
 */
@ComponentScan
public class RequestJDBCTemplate {
    private SimpleDriverDataSource dataSource;
    @Autowired
    @Qualifier("jdbcTemplateObject")
    private JdbcTemplate jdbcTemplateObject;
    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String method, String url, String httpVersion, int postDataId,
                       int headerSize, int bodySize, String comment) {
        String SQL = "insert into requests(method, url, http_version, post_data_id, header_size, body_size, comment) values (?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplateObject.update( SQL, method, url, httpVersion, postDataId, headerSize, bodySize, comment);
        return;
    }
    public int getId(){
        String sql = "select max(id) from requests";
        int i= jdbcTemplateObject.queryForObject(sql, Integer.class);
        return i;
    }
    public Request getParam(Integer id) {
        String SQL = "select * from requests where id = ?";
        Request request = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new RequestMapper());
        return request;
    }
    public List<Request> listRequests() {
        String SQL = "select * from requests";
        List <Request> requests = jdbcTemplateObject.query(SQL,
                new RequestMapper());
        return requests;
    }
}
