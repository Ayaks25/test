package DAO;

import java.util.List;

import domains.PostData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.PostDataMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
/**
 * Created by Алексей on 31.03.2016.
 */
@ComponentScan
public class PostDataJDBCTemplate {
    private SimpleDriverDataSource dataSource;
    @Autowired
    @Qualifier("jdbcTemplateObject")
    private JdbcTemplate jdbcTemplateObject;
    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public int getId(){
        String sql = "select max(id) from post_data";
        int i= jdbcTemplateObject.queryForObject(sql, Integer.class);
        return i;
    }
    public void create(String mimeType, String text, String comment) {
        String SQL = "insert into post_data(mime_type, text, comment) values (?, ?, ?)";
        jdbcTemplateObject.update( SQL, mimeType, text, comment);
        return;
    }
    public PostData getPostData(Integer id) {
        String SQL = "select * from post_data where id = ?";
        PostData postData = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new PostDataMapper());
        return postData;
    }
    public List<PostData> listPostData() {
        String SQL = "select * from post_data";
        List <PostData> postDatas = jdbcTemplateObject.query(SQL,
                new PostDataMapper());
        return postDatas;
    }
    public void delete(Integer id) {
        String SQL = "delete from post_data where id = ?";
        jdbcTemplateObject.update(SQL, id);
        return;
    }
}
