package DAO;

import java.util.List;
import domains.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.ParamMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
/**
 * Created by Алексей on 31.03.2016.
 */
@ComponentScan
public class ParamJDBCTemplate {
    private SimpleDriverDataSource dataSource;
    @Autowired
    @Qualifier("jdbcTemplateObject")
    private JdbcTemplate jdbcTemplateObject;
    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String name, String value, String fileName, String contentType,
                       String comment, int postDataId) {
        String SQL = "insert into params(name, value, file_name, content_type, comment, post_data_id) values (?, ?, ?, ?, ?, ?)";
        jdbcTemplateObject.update( SQL, name, value, fileName, contentType, comment, postDataId);
        return;
    }
    public Param getParam(Integer id) {
        String SQL = "select * from params where id = ?";
        Param param = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new ParamMapper());
        return param;
    }
    public List<Param> listParams() {
        String SQL = "select * from params";
        List <Param> params = jdbcTemplateObject.query(SQL,
                new ParamMapper());
        return params;
    }
}


