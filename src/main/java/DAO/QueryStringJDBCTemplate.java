package DAO;

import java.util.List;
import domains.QueryString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.QueryStringMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
/**
 * Created by Алексей on 31.03.2016.
 */
@ComponentScan
public class QueryStringJDBCTemplate {
    private SimpleDriverDataSource dataSource;
    @Autowired
    @Qualifier("jdbcTemplateObject")
    private JdbcTemplate jdbcTemplateObject;
    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String name, String value, String comment, int reqId) {
        String SQL = "insert into query_string(name, value, comment, req_id) values (?, ?, ?, ?)";
        jdbcTemplateObject.update( SQL, name, value, comment,reqId);
        return;
    }
    public QueryString getQueryString(Integer id) {
        String SQL = "select * from query_string where id = ?";
        QueryString queryString = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new QueryStringMapper());
        return queryString;
    }
    public List<QueryString> listQueryString() {
        String SQL = "select * from query_string";
        List <QueryString> queryStrings = jdbcTemplateObject.query(SQL,
                new QueryStringMapper());
        return queryStrings;
    }
}

