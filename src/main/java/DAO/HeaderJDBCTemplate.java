package DAO;

import java.util.List;
import domains.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.HeaderMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
    /**
     * Created by Алексей on 31.03.2016.
     */
    @ComponentScan
    public class HeaderJDBCTemplate {
        private SimpleDriverDataSource dataSource;
        @Autowired
        @Qualifier("jdbcTemplateObject")
        private JdbcTemplate jdbcTemplateObject;
        public void setDataSource(SimpleDriverDataSource dataSource) {
            this.dataSource = dataSource;
            this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        }
        public void create(String name, String value, String comment, int reqId) {
            String SQL = "insert into headers(name, value, comment, req_id) values (?, ?, ?, ?)";
            jdbcTemplateObject.update( SQL, name, value, comment,reqId);
            return;
        }
        public Header getHeader(Integer id) {
            String SQL = "select * from headers where id = ?";
            Header header = jdbcTemplateObject.queryForObject(SQL,
                    new Object[]{id}, new HeaderMapper());
            return header;
        }
        public List<Header> listHeaders() {
            String SQL = "select * from headers";
            List <Header> headers = jdbcTemplateObject.query(SQL,
                    new HeaderMapper());
            return headers;
        }
    }

