package DAO;

import java.util.List;

import domains.Cookie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import util.CookieMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
/**
 * Created by Алексей on 31.03.2016.
 */
@ComponentScan
public class CookieJDBCTemplate {
    private SimpleDriverDataSource dataSource;
    @Autowired
    @Qualifier("jdbcTemplateObject")
    private JdbcTemplate jdbcTemplateObject;
    public void setDataSource(SimpleDriverDataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
    }
    public void create(String name, String value, String path, String domain, String expires,
                       boolean httpOnly, boolean secure, String comment, int reqId) {
        String SQL = "insert into cookies(name, value, path, domain, expires, http_only, secure, comment, req_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplateObject.update( SQL, name, value, path,  domain,  expires, httpOnly, secure, comment,reqId);
        return;
    }
    public Cookie getCookie(Integer id) {
        String SQL = "select * from cookies where id = ?";
        Cookie cookie = jdbcTemplateObject.queryForObject(SQL,
                new Object[]{id}, new CookieMapper());
        return cookie;
    }
    public List<Cookie> listCookies() {
        String SQL = "select * from cookies";
        List <Cookie> cookies = jdbcTemplateObject.query(SQL,
                new CookieMapper());
        return cookies;
    }
}
