        package com;
        import java.io.*;
        import javax.servlet.*;
        import javax.servlet.http.HttpServletRequest;
        import org.apache.commons.mail.*;

/**
 *
 * @author Алексей
 */
public class ReqFilter implements Filter {
    public void sendEmail() throws EmailException {
        Email email = new SimpleEmail();
        email.setHostName("smtp.yandex.ru");
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator("Gregosbgenth", "GlkIRhb12iosHQSo"));
        email.setSSLOnConnect(true);
        email.setFrom("Gregosbgenth@yandex.ru");
        email.setSubject("Error");
        email.setMsg("PUT Error /hello :-)");
        email.addTo("test-webserver@mailinator.com");
        email.send();
    }
    public void init(FilterConfig filterConfig) throws ServletException
    {

    }
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if(httpRequest.getMethod().equalsIgnoreCase("PUT")){
            try {
                sendEmail();
            } catch (EmailException e) {
                e.printStackTrace();
            }
            RequestDispatcher view = request.getRequestDispatcher("Error.jsp");
            view.forward(request,response);
        }
        filterChain.doFilter(request, response);
    }

    public void destroy()
    {
    }
}

