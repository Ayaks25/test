package com;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class Servlet extends HttpServlet{

    public void doPost(HttpServletRequest request,
                       HttpServletResponse response )
            throws IOException, ServletException {
        RequestDispatcher view = request.getRequestDispatcher("result.jsp");
        view.forward(request,response);

    }
    //public void doGet(HttpServletRequest request,
      //                HttpServletResponse response )
        //    throws IOException, ServletException {
        //RequestDispatcher view = request.getRequestDispatcher("/hello/index.jsp");
        //view.forward(request,response);
    //}
}
