package com;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

import domains.PostDataJDBCTemplate;
import domains.PostData;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

public class JDBCServlet extends HttpServlet{
    public void doPost(HttpServletRequest request,
                       HttpServletResponse response )
            throws IOException, ServletException {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        PostDataJDBCTemplate postData = (PostDataJDBCTemplate) ctx.getBean("postData");
        SimpleDriverDataSource dataSource=(SimpleDriverDataSource) ctx.getBean("dataSource");
        postData.setDataSource(dataSource);
        PostData data = postData.getPostData(1);
        request.getSession().setAttribute("data",data);
        RequestDispatcher view = request.getRequestDispatcher("jdbc.jsp");
        view.forward(request,response);

    }
    public void doGet(HttpServletRequest request,
                    HttpServletResponse response )
      throws IOException, ServletException {
    RequestDispatcher view = request.getRequestDispatcher("/hello/index.jsp");
    view.forward(request,response);
    }
}