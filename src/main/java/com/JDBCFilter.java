package com;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.Enumeration;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;

import DAO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

/**
 * Created by Алексей on 01.04.2016.
 */
@ComponentScan
public class JDBCFilter implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException
    {
        HttpServletRequest requestf = (HttpServletRequest) request;
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfiguration.class);
        PostDataJDBCTemplate postData = (PostDataJDBCTemplate) ctx.getBean("postData");
        CookieJDBCTemplate cookie =(CookieJDBCTemplate) ctx.getBean("cookie");
        HeaderJDBCTemplate header = (HeaderJDBCTemplate) ctx.getBean("header");
        ParamJDBCTemplate param = (ParamJDBCTemplate) ctx.getBean("param");
        QueryStringJDBCTemplate queryString = (QueryStringJDBCTemplate) ctx.getBean("queryString");
        RequestJDBCTemplate req = (RequestJDBCTemplate) ctx.getBean("req");
        //Запись в таблицу post_data
        postData.create(request.getContentType(),"","");
        //Запись в таблицу requests
        req.create(requestf.getMethod(), requestf.getRequestURI(), requestf.getProtocol(),postData.getId(),0, requestf.getContentLength(),"");
        //Запись в таблицу cookies
        Cookie[] cookies = requestf.getCookies();
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                cookie.create(cookies[i].getName(),cookies[i].getValue(), cookies[i].getPath(),cookies[i].getDomain(),
                        "", false, cookies[i].getSecure(),cookies[i].getComment(),req.getId());
            }
        }
        //Запись в таблицу headers
        Enumeration headers=requestf.getHeaderNames();
        while (headers.hasMoreElements()) {
            String s=(String)headers.nextElement();
            header.create(s,requestf.getHeader(s),"",req.getId());
        }
        //Запись в таблицу query_string
        queryString.create(requestf.getRequestURI(),requestf.getQueryString(),"",req.getId());
        //Запись в таблицу params
        Enumeration params=requestf.getParameterNames();
        while (params.hasMoreElements()){
            String s= (String)params.nextElement();
            param.create(s,requestf.getParameter(s),"",requestf.getContentType(),"",postData.getId());
        }
        filterChain.doFilter(request, response);
    }

    public void destroy()
    {
    }
}
