package com;

import DAO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.activation.DataSource;

/**
 * Created by Алексей on 31.03.2016.
 */
@Configuration
@ComponentScan
public class SpringConfiguration {
    @Bean(name="dataSource")
    SimpleDriverDataSource simpleDriverDataSource(){
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.postgresql.Driver.class);
        dataSource.setUsername("postgres");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/Web_app");
        dataSource.setPassword("admin123");
        return dataSource;
    }
    @Bean(name = "jdbcTemplateObject")
    JdbcTemplate jdbcTemplate(){
        SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(org.postgresql.Driver.class);
        dataSource.setUsername("postgres");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/Web_app");
        dataSource.setPassword("admin123");
        return new JdbcTemplate(dataSource);}
    @Bean(name="postData")
    PostDataJDBCTemplate postDataJDBCTemplate(){
        return new PostDataJDBCTemplate();
    }
    @Bean(name="req")
    RequestJDBCTemplate requestJDBCTemplate(){return new RequestJDBCTemplate();}
    @Bean(name = "cookie")
    CookieJDBCTemplate cookieJDBCTemplate() {return new CookieJDBCTemplate();}
    @Bean(name = "header")
    HeaderJDBCTemplate headerJDBCTemplate() {return new HeaderJDBCTemplate();}
    @Bean(name = "param")
    ParamJDBCTemplate paramJDBCTemplate(){return new ParamJDBCTemplate();}
    @Bean(name = "queryString")
    QueryStringJDBCTemplate queryStringJDBCTemplate(){return new QueryStringJDBCTemplate();}
}
