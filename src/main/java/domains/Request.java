package domains;

import java.io.Serializable;

/**
 * Created by ������� on 30.03.2016.
 */

public class Request implements Serializable {
    private int id;
    private String method;
    private String url;
    private String httpVersion;
    private int postDataId;
    private int headerSize;
    private int bodySize;
    private String comment;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public void setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
    }

    public int getPostDataId() {
        return postDataId;
    }

    public void setPostDataId(int postDataId) {
        this.postDataId = postDataId;
    }

    public int getHeaderSize() {
        return headerSize;
    }

    public void setHeaderSize(int headerSize) {
        this.headerSize = headerSize;
    }

    public int getBodySize() {
        return bodySize;
    }

    public void setBodySize(int bodySize) {
        this.bodySize = bodySize;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    public Request()
    {

    }
    public Request( int id, String method, String url, String httpVersion,
             int postDataId, int headerSize, int bodySize, String comment)
    {
        this.id=id;
        this.method=method;
        this.url=url;
        this.httpVersion=httpVersion;
        this.postDataId=postDataId;
        this.headerSize=headerSize;
        this.bodySize=bodySize;
        this.comment=comment;
    }

}
