package domains;

import java.io.Serializable;

/**
 * Created by Алексей on 30.03.2016.
 */
public class Param implements Serializable {
    private int id;
    private String name;
    private String value;
    private String fileName;
    private String contentType;
    private String comment;
    private int postDataId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getPostDataId() {
        return postDataId;
    }

    public void setPostDataId(int postDataId) {
        this.postDataId = postDataId;
    }
}
