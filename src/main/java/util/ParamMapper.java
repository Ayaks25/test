package util;

/**
 * Created by Алексей on 01.04.2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import domains.Param;
import org.springframework.jdbc.core.RowMapper;


public class ParamMapper implements RowMapper<Param> {
    public Param mapRow(ResultSet rs, int rowNum) throws SQLException {
        Param param = new Param();
        param.setId(rs.getInt("id"));
        param.setName(rs.getString("name"));
        param.setValue(rs.getString("value"));
        param.setFileName(rs.getString("file_name"));
        param.setContentType(rs.getString("content_type"));
        param.setComment(rs.getString("comment"));
        param.setPostDataId(rs.getInt("post_data_id"));
        return param;
    }

}
