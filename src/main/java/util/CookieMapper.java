package util;

/**
 * Created by Алексей on 01.04.2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import domains.Cookie;
import org.springframework.jdbc.core.RowMapper;


    public class CookieMapper implements RowMapper<Cookie> {
        public Cookie mapRow(ResultSet rs, int rowNum) throws SQLException {
            Cookie cookie = new Cookie();
            cookie.setId(rs.getInt("id"));
            cookie.setName(rs.getString("name"));
            cookie.setValue(rs.getString("value"));
            cookie.setPath(rs.getString("path"));
            cookie.setDomain(rs.getString("domain"));
            cookie.setExpires(rs.getString("expires"));
            cookie.setHttpOnly(rs.getBoolean("http_only"));
            cookie.setSecure(rs.getBoolean("secure"));
            cookie.setComment(rs.getString("comment"));
            cookie.setReqId(rs.getInt("req_id"));
            return cookie;
        }

    }
