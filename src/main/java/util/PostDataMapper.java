package util;

import java.sql.ResultSet;
import java.sql.SQLException;

import domains.PostData;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by Алексей on 31.03.2016.
 */
public class PostDataMapper implements RowMapper<PostData> {
    public PostData mapRow(ResultSet rs, int rowNum) throws SQLException {
        PostData postData = new PostData();
        postData.setId(rs.getInt("id"));
        postData.setMimeType(rs.getString("mime_type"));
        postData.setText(rs.getString("text"));
        postData.setComment(rs.getString("comment"));
        return postData;
    }

    }
