package util;

/**
 * Created by Алексей on 01.04.2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import domains.QueryString;
import org.springframework.jdbc.core.RowMapper;


public class QueryStringMapper implements RowMapper<QueryString> {
    public QueryString mapRow(ResultSet rs, int rowNum) throws SQLException {
        QueryString queryString = new QueryString();
        queryString.setId(rs.getInt("id"));
        queryString.setName(rs.getString("name"));
        queryString.setValue(rs.getString("value"));
        queryString.setComment(rs.getString("comment"));
        queryString.setReqId(rs.getInt("req_id"));
        return queryString;
    }

}