package util;

/**
 * Created by Алексей on 01.04.2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import domains.Header;
import org.springframework.jdbc.core.RowMapper;


public class HeaderMapper implements RowMapper<Header> {
    public Header mapRow(ResultSet rs, int rowNum) throws SQLException {
        Header header = new Header();
        header.setId(rs.getInt("id"));
        header.setName(rs.getString("name"));
        header.setValue(rs.getString("value"));
        header.setComment(rs.getString("comment"));
        header.setReqId(rs.getInt("req_id"));
        return header;
    }

}
