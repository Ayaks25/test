package util;

/**
 * Created by Алексей on 01.04.2016.
 */
import java.sql.ResultSet;
import java.sql.SQLException;

import domains.Request;
import org.springframework.jdbc.core.RowMapper;


public class RequestMapper implements RowMapper<Request> {
    public Request mapRow(ResultSet rs, int rowNum) throws SQLException {
        Request request = new Request();
        request.setId(rs.getInt("id"));
        request.setMethod(rs.getString("method"));
        request.setUrl(rs.getString("url"));
        request.setHttpVersion(rs.getString("http_version"));
        request.setPostDataId(rs.getInt("post_data_id"));
        request.setHeaderSize(rs.getInt("header_size"));
        request.setBodySize(rs.getInt("body_size"));
        request.setComment(rs.getString("comment"));
        return request;
    }

}
